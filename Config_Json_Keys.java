package wjmartz.com.wjmartz.config;

/**
 * Created by softeligent on 7/18/2017.
 */

public class Config_Json_Keys {
   public static String main_url = "http://www.wjmartz.com/app-wjmartz/index.php/";
    public static String base_url = "imageaccesspath";
    public static String deal_status_str = "deal_status";
    /*****************************************************
     * Parent Category List url with variables
     *************************************************/
    public static String parent_category_list_url = main_url + "aparent_categories/get_all_parent_categories";
    public static String parent_category_list_array = "parent_categories";
    public static String parent_category_id = "parent_cat_id";
    public static String parent_category_name = "parent_cat_name";
    public static String parent_category_img_url = "parent_cat_img_url";


    /*Sub category url with variables*/
    public static String child_category_list_url = main_url + "achild_categories/get_child_category_list/";
    public static String child_category_list_array = "child_cat_list";
    public static String child_cat_id = "child_cat_id";
    public static String child_cat_name = "child_cat_name";
    public static String child_cat_img_url = "child_cat_img_url";

    /*Product list url with variables*/
    public static String product_list_url = main_url + "aproduct/general_product_list/";
    public static String product_list_array_str = "products";
    public static String prod_id = "prod_id";
    public static String prod_name = "prod_name";
    public static String prod_img_url = "prod_img_url";
    public static String prod_price = "prod_price";
    public static String prod_description = "prod_description";
    public static String prod_weight = "quantity";
    public static String prod_type = "product_type";
    public static String prod_deal_status = "deal_status";
    /*****************************************************
     * Proceed to checkout
     *************************************************/
    /*User list url and variables*/
    public static String customer_address_list = main_url + "acustomer/address_list/";
    public static String customer_address_array = "customer_address";
    public static String customer_address_id = "address_id";
    public static String customer_address = "address";
    public static String customer_address_state = "state";
    public static String customer_address_city = "city";
    public static String customer_address_area = "area_name";
    public static String customer_address_pincode = "pincode";

    /*Sending time and date for fetching time slots*/
    public static String checkout_date_time_slot_sending = main_url + "aorder/get_time_slots";
    public static String checkout_date_time_slot_current_time = "current_time";
    public static String checkout_date_time_slot_delivery_date = "delivery_date";
    public static String checkout_date_time_slot_product_type = "product_type";

    public static String time_slot_object = "time_slot";
    public static String select_time = "select_time";
    public static String list_array = "list";
    public static String time_slot_id = "time_slot_id";
    public static String time_slot = "time_slot";


    /*Generating reference id*/
    public static String get_reference_order_data = main_url + "aorder/get_reference_order_data";
    public static String get_reference_order_user_id = "user_id";
    public static String get_reference_order_address_id = "address_id";
    public static String get_reference_order_current_time = "current_time";
    public static String get_reference_order_time_slot_id = "time_slot_id";
    public static String get_reference_order_product_details = "product_details";
    public static String get_reference_order_delivery_charges = "delivery_charges";
    public static String get_reference_order_reference_name = "reference_name";
    public static String get_reference_order_remark = "remark";
    public static String get_reference_order_promo_id = "promo_id";
    public static String get_reference_order_discount = "discount";
    public static String get_reference_order_fname = "first_name";
    public static String get_reference_order_lname = "last_name";
    public static String get_reference_order_promo_type_id = "promo_type_id";
    /*Getting reference id with data*/
    public static String fetch_customer_data_details_array = "customer_data";
    public static String fetch_customer_data_ref_order_id = "ref_order_id";
    public static String fetch_customer_data_cust_id = "cust_id";
    public static String fetch_customer_data_first_name = "first_name";
    public static String fetch_customer_data_last_name = "last_name";
    public static String fetch_customer_data_email = "email";
    public static String fetch_customer_data_mobile = "mobile";
    public static String fetch_customer_data_address = "address";
    public static String fetch_customer_data_area_name = "area_name";
    public static String fetch_customer_data_city = "city";
    public static String fetch_customer_data_state = "state";
    public static String fetch_customer_data_pincode = "pincode";
    public static String fetch_customer_data_order_date = "order_date";
    public static String fetch_customer_data_discount_amount = "discount_amount";

    public static String fetch_customer_data_promocode = "promocode";
    public static String fetch_customer_data_discount = "discount";
    public static String fetch_customer_data_amount = "amount";
    public static String fetch_customer_data_delivery_charges = "delivery_charges";
    public static String fetch_customer_product_data = "product_data";
    public static String fetch_customer_prod_name = "prod_name";
    public static String fetch_customer_prod_img_url = "prod_image_url";
    public static String fetch_customer_price = "price";
    public static String fetch_customer_prod_size = "size";
    public static String fetch_customer_quantity = "quantity";

    /*****************************************************
     * Fetch Delivery charges
     *************************************************/
    public static String fetch_delivery_charges_url = main_url + "aorder/get_pincodewise_delivery_charges";
    public static String fetch_delivery_cust_id = "cust_id";
    public static String fetch_delivery_address_id = "address_id";
    public static String fetch_delivery_delivery_chargesobj = "delivery_charges";
    public static String fetch_delivery_delivery_charges = "delivery_charges";
    public static String fetch_delivery_pincode = "pincode";
    public static String fetch_delivery_area_name = "area_name";

    /*****************************************************
     * Searching data in searchview
     *************************************************/

    public static String searchview_url = main_url + "amain_categories/search";
    public static String getSearchview_search_keyword = "search_keyword";
    public static String searchview_fetch_result = "result";
    public static String searchview_fetch_product = "product_details";
    public static String searchview_fetch_prod_name = "prod_name";
    public static String searchview_fetch_prod_id = "prod_id";
    public static String searchview_fetch_data_pccat_id = "pccat_id";
    public static String searchview_fetch_data_fk_pcolor_id = "fk_pcolor_id";
    public static String searchview_fetch_deal_status = "deal_status";

    public static String searchview_fetch_data_category_array = "category";
    public static String searchview_fetch_data_child_cat_name = "child_cat_name";
    public static String searchview_fetch_data_child_cat_id = "child_cat_id";

    /*****************************************************
     * Searching data in searchview products
     *************************************************/
    public static String searchview_fetch_products_url = main_url + "amain_categories/search_product_details/";
    public static String searchview_fetch_product_details = "product_details";
    public static String searchview_fetch_product_id = "prod_id";
    public static String searchview_fetch_pccat_id = "pccat_id";
    public static String searchview_fetch_fk_pcolor_id = "fk_pcolor_id";
    public static String searchview_fetch_fk_size_id = "fk_psize_ids";
    public static String searchview_fetch_product_name = "prod_name";
    public static String searchview_fetch_product_price = "prod_price";
    public static String searchview_fetch_product_img_url = "prod_image_url";
    public static String searchview_fetch_SIZE_ARRAY = "psize_list";
    public static String searchview_fetch_SIZE_ID = "size_id";
    public static String searchview_fetch_SIZE = "size";
    /*****************************************************
     * customer login form
     *************************************************/
    //url
    public static String login_form_url = main_url + "acustomer/customer_login_with_otp";
    //variables
    public static String login_form_mobile_number = "mobile_number";

    /*****************************************************
     * customer registration signup form
     *************************************************/
    public static String signup_form_url = main_url + "acustomer/new_customer_registration";
    public static String signup_form_firstname = "firstname";
    public static String signup_form_lastname = "lastname";
    public static String signup_form_mobile = "mobile";
    public static String signup_form_email = "email";
    public static String signup_form_address = "address";
    public static String signup_form_area = "area";
    public static String signup_form_pincode_id = "pincode_id";
    public static String signup_form_dob = "dob";

    /*****************************************************
     * Signup spinner fetching data
     *************************************************/


    public static String fetch_signup_dropdown_url = main_url + "acustomer/address_listing";
    //state
    public static String fetch_state_list = "state_list";
    public static String fetch_state_select = "select_state";
    public static String fetch_state_list_array = "list";
    public static String fetch_state_id = "state_id";
    public static String fetch_state_name = "state";

    //city
    public static String fetch_city_list = "city_list";
    public static String fetch_city_select = "select_city";
    public static String fetch_city_list_array = "list";
    public static String fetch_city_state_id = "state_id";
    public static String fetch_city_id = "city_id";
    public static String fetch_city_name = "city";

    //pincode
    public static String fetch_pincode_list = "pincode_list";
    public static String fetch_pincode_list_array = "list";
    public static String fetch_pincode_select = "select_pincode";
    public static String fetch_pincode_id = "pincode_id";
    public static String fetch_pincode_name = "pincode";
    public static String fetch_pincode_city_id = "city_id";

    /*****************************************************
     * customer update profile fetch data
     *************************************************/
    public static String update_profile_fetch_data_url = main_url + "acustomer/customer_data_for_update_profile/";
    public static String update_profile_fetch_data_array = "customer_data";
    public static String update_profile_fetch_data_first_name = "first_name";
    public static String update_profile_fetch_data_last_name = "last_name";
    public static String update_profile_fetch_data_email = "email";
    public static String update_profile_fetch_data_mobile = "mobile";
    public static String update_profile_fetch_data_address1_obj = "address1";
    public static String update_profile_fetch_data_address2_obj = "address2";
    public static String update_profile_fetch_data_address3_obj = "address3";
    public static String update_profile_fetch_data_address = "address";
    public static String update_profile_fetch_data_area_name = "area_name";
    public static String update_profile_fetch_data_city = "city";
    public static String update_profile_fetch_data_state = "state";
    public static String update_profile_fetch_data_pincode = "pincode";
    public static String update_profile_fetch_data_address_id = "address_id";
    public static String update_profile_fetch_data_state_id = "state_id";
    public static String update_profile_fetch_data_city_id = "city_id";
    public static String update_profile_fetch_data_area_id = "area_id";
    public static String update_profile_fetch_data_pincode_id = "pincode_id";
    public static String update_profile_fetch_data_dob = "dob";
    public static String update_profile_fetch_data_user_photo = "user_photo";
    //state
    public static String update_profile_fetch_state_list = "state_list";
    public static String update_profile_fetch_state_select = "select_state";
    public static String update_profile_fetch_state_list_array = "list";
    public static String update_profile_fetch_state_id = "state_id";
    public static String update_profile_fetch_state_name = "state";

    //city
    public static String update_profile_fetch_city_list = "city_list";
    public static String update_profile_fetch_city_select = "select_city";
    public static String update_profile_fetch_city_list_array = "list";
    public static String update_profile_fetch_city_state_id = "state_id";
    public static String update_profile_fetch_city_id = "city_id";
    public static String update_profile_fetch_city_name = "city";


    //pincode
    public static String update_profile_fetch_pincode_list = "pincode_list";
    public static String update_profile_fetch_pincode_list_array = "list";
    public static String update_profile_fetch_pincode_select = "select_pincode";
    public static String update_profile_fetch_pincode_id = "pincode_id";
    public static String update_profile_fetch_pincode_name = "pincode";
    public static String update_profile_fetch_pincode_city_id = "city_id";


    /*****************************************************
     * customer update profile send data
     *************************************************/
    public static String update_profile_send_data_url = main_url + "acustomer/update_customer_name";
    public static String update_profile_send_cust_id = "cust_id";

    /*Name*/
    public static String update_profile_send_data_url_name = main_url + "acustomer/update_customer_name";
    public static String update_profile_send_data_first_name = "first_name";
    public static String update_profile_send_data_last_name = "last_name";

    /*Birthdate*/
    public static String update_profile_send_data_url_dob = main_url + "acustomer/update_customer_birthdate";
    public static String update_profile_send_data_dob = "dob";

    /*Email Address*/
    public static String update_profile_send_data_url_email = main_url + "acustomer/update_customer_email";
    public static String update_profile_send_data_email = "email";

    /*Mobile number*/
    public static String update_profile_send_data_url_mobile_for_otp = main_url + "acustomer/generate_otp_for_update_mobile";
    public static String update_profile_send_data_url_mobile_with_otp = main_url + "acustomer/update_mobile_with_otp";
    public static String update_profile_send_data_mobile = "mobile";
    public static String update_profile_send_mobile_response_status = "status";
    public static String update_profile_send_mobile_response_otp = "otp";

    /*Address*/
    public static String update_profile_send_data_address_id = "address_id";
    /*Address1*/
    public static String update_profile_send_data_address1_url = main_url + "acustomer/update_customer_address1";
    public static String update_profile_send_data_address = "address1";
    public static String update_profile_send_data_pincode_id = "pincode1";
    public static String update_profile_send_data_area_id = "area1";

    /*Address2*/
    public static String update_profile_send_data_address2_url = main_url + "acustomer/update_customer_address2";
    public static String update_profile_send_data_address2 = "address2";
    public static String update_profile_send_data_area_id2 = "area2";
    public static String update_profile_send_data_pincode_id2 = "pincode2";

    /*Address3*/
    public static String update_profile_send_data_address3_url = main_url + "acustomer/update_customer_address3";
    public static String update_profile_send_data_address3 = "address3";
    public static String update_profile_send_data_area_id3 = "area3";
    public static String update_profile_send_data_pincode_id3 = "pincode3";

    /*User Image*/
    public static String update_profile_send_data_user_photo_url = main_url + "acustomer/update_customer_photo";
    public static String update_profile_send_data_user_photo = "user_photo";

    /*Add Address2*/
    public static String add_address2_url = main_url + "acustomer/insert_address2";
    public static String add_address3_url = main_url + "acustomer/insert_address3";

    /*****************************************************
     * Home screen slider
     *************************************************/
    public static String url = main_url + "amain_categories/slider_images";
    public static String slider_array_name = "sliders";
    public static String slider_imageaccesspath = "imageaccesspath";
    public static String slider_image_url = "image_path";

    /*****************************************************
     * Home screen parent categories
     *************************************************/
    public static String home_parent_category = main_url + "aparent_categories/get_section_parent_categories";
    public static String home_parent_categories_array = "sec_parent_cat";
    public static String home_screen_parent_cat_id = "parent_cat_id";
    public static String home_screen_parent_cat_name = "parent_cat_name";
    public static String home_screen_parent_cat_img_url = "parent_cat_img_url";


    /******************************************************
     * PayBiz Integration And Success and Failure Implemaentation
     ********************************************/
    /*PAYUBiz URLS*/
  /*PAYUBiz URLS*/
    public static String product_payment_credentials = main_url + "apayment/get_product_payment_credentials";
    public static String plan_payment_reverse_hash = main_url + "Apayment/reverse_hash";
    public static String getPlan_payment_reverse_hash_str = "payu_data";

    /*Surl and Furl pass to payudata*/
    public static String success_url = main_url + "aorder/payment_success";
    public static String failure_url = main_url + "aorder/payment_fail";


    /*Fetch Membership Data on Success*/
    public static String purchase_send_payudata_success_url = main_url + "aorder/get_order_data_success";
    public static String purchase_send_payu_data_success = "payu_data";
    public static String purchase_success_Member_Data_obj = "order_data";
    public static String purchase_success_cust_id = "cust_id";
    public static String purchase_success_transaction_id = "transaction_id";
    public static String purchase_success_order_id = "order_id";
    public static String purchase_success_payment_datetime = "payment_datetime";
    public static String purchase_success_payment_status = "payment_status";
    public static String purchase_success_amount = "discount_amount";
    public static String purchase_success_discount_amount = "discount_amount";
    public static String purchase_success_firstname = "first_name";
    public static String purchase_success_lastname = "last_name";
    public static String purchase_success_email = "email";
    public static String purchase_success_mobile = "mobile";

    /*Fetch Failure Data On Failure*/
    public static String purchase_send_payudata_fail = main_url + "aorder/get_order_data_failure";
    public static String purchase_send_payu_data_fail = "payu_data";
    public static String purchase_fail_Member_Data_obj_fail = "failure_data";
    public static String purchase_fail_reference_id = "ref_order_id";
    public static String purchase_fail_transaction_id = "transaction_id";
    public static String purchase_fail_payment_datetime = "payment_datetime";
    public static String purchase_fail_payment_status = "payment_status";
    public static String purchase_fail_reason = "trans_reason";
    public static String purchase_fail_amount = "discount_amount";
    public static String purchase_fail_firstname = "first_name";
    public static String purchase_fail_lastname = "last_name";
    public static String purchase_fail_email = "email";
    public static String purchase_fail_mobile = "mobile";

    /*Payubiz END*/

    /*****************************************************
     * Order History
     *************************************************/
    public static String order_history_url = main_url + "aorder/order_history/";
    public static String order_history_successs_array = "order_history_successs";
    public static String order_history_successs_order_id = "order_id";
    public static String order_history_order_date = "order_date";
    public static String order_history_amount = "discount_amount";
    public static String order_history_discount_amount = "discount_amount";
    public static String order_history_order_status = "order_status";
    public static String order_history_payment_status = "payment_status";
    public static String order_history_failure_array = "order_history_failure";
    public static String order_history_failure_ref_order_id = "ref_order_id";
    public static String order_history_refund_id = "refund_id";
    /*****************************************************
     * Order History Details
     *************************************************/
    public static String order_history_deatils_url = main_url + "aorder/order_history_details/";
    public static String order_history_deatils_product_list_url = main_url + "aorder/order_history_product_details/";
    public static String order_history_refund_product_list_url = main_url + "aorder/get_refunded_product_list/";
    public static String order_history_deatils_product = "order_history_products";
    public static String order_history_deatils_product_id = "product_id";
    public static String order_history_deatils_prod_name = "prod_name";
    public static String order_history_deatils_prod_img_url = "prod_image_url";
    public static String order_history_deatils_price = "price";
    public static String order_history_deatils_amount = "amount";
    public static String order_history_deatils_quantity = "quantity";
    public static String order_history_deatils_size = "size";
    public static String order_history_deatils_size_id = "size_id";
    public static String order_history_deatils_sgst_amount = "sgst_amount";
    public static String order_history_deatils_cgst_amount = "cgst_amount";
    public static String order_history_deatils_refund_status = "refund_status";

    public static String order_history_details_array = "order_history_details";
    public static String order_history_details_order_id = "order_id";
    public static String order_history_details_ref_order_id = "ref_order_id";
    public static String order_history_details_cust_id = "cust_id";
    public static String order_history_details_transaction_id = "transaction_id";
    public static String order_history_details_first_name = "first_name";
    public static String order_history_details_last_name = "last_name";
    public static String order_history_details_email = "email";
    public static String order_history_details_mobile = "mobile";
    public static String order_history_details_address = "address";
    public static String order_history_details_area_name = "area_name";
    public static String order_history_details_city = "city";
    public static String order_history_details_state = "state";
    public static String order_history_details_pincode = "pincode";
    public static String order_history_details_date = "delivery_date";
    public static String order_history_details_time_slot = "time_slot";
    public static String order_history_details_order_status = "order_status";
    public static String order_history_details_order_status_id = "ord_status_id";
    public static String order_history_details_payment_status = "payment_status";
    public static String order_history_details_delivery_charges = "delivery_charges";
    public static String order_history_details_promocode = "promocode";

    /*****************************************************
     * Cancel Order
     *************************************************/
    public static String order_cancel_order_url = main_url + "aorder/cancel_order";
    public static String order_cancel_order_id = "order_id";
    public static String order_cancel_customer_id = "customer_id";
    /*****************************************************
     * Drawer data
     *************************************************/
    public static String drawer_data_url = main_url + "acustomer/customer_data_for_drawer/";
    public static String drawer_data_array = "customer_data";
    public static String drawer_data_cust_id = "cust_id";
    public static String drawer_data_first_name = "first_name";
    public static String drawer_data_last_name = "last_name";
    public static String drawer_data_user_photo = "user_photo";


/*Categories women wear*/

    public static String child_cat_list_url = main_url + "achild_categories/get_child_category_list/";
    public static String child_cat_list_child_cat_list_array = "child_cat_list";
    public static String child_cat_list_child_cat_id = "child_cat_id";
    public static String child_cat_list_child_cat_name = "child_cat_name";
    public static String child_cat_list_child_cat_img_url = "child_cat_img_url";


    /*Men Full Sleeve Tshirts*/
    public static String WOMEN_WEAR_URL = main_url + "aproduct/product_list/";
    public static String WOMEN_WEAR_URL_HIGH_TO_LOW_PRICE = main_url + "aproduct/high_price_filtration/";
    public static String WOMEN_WEAR_URL_LOW_TO_HIGH_PRICE = main_url + "aproduct/low_price_filtration/";
    public static String WOMEN_WEAR_ARRAY = "products";
    public static String WOMEN_WEAR_PRODUCT_IMAGE_URL = "image_url";
    public static String WOMEN_WEAR_PID = "pid";
    public static String WOMEN_WEAR_PNAME = "pname";
    public static String WOMEN_WEAR_PCCAT_ID = "pccat_id";
    public static String WOMEN_WEAR_FKP_SIZE_ID = "fk_psize_ids";
    public static String WOMEN_WEAR_DISCOUNT = "discount";
    public static String WOMEN_WEAR_MRP = "mrp";
    public static String WOMEN_WEAR_PRICE = "price";
    public static String WOMEN_WEAR_SIZE_ARRAY = "psize_list";
    public static String WOMEN_WEAR_SIZE_ID = "size_id";
    public static String WOMEN_WEAR_SIZE = "size";
    public static String WOMEN_WEAR_COLOR_ID = "fk_pcolor_id";
    public static String WOMEN_WEAR_BASE_PCCAT_ID = "pccat_id";

    /*Product details*/
    public static String PRODUCT_DETAILS = main_url + "aproduct/product_details/";


    /*Filter Color*/
    public static String PCOLOR_LIST_URL = main_url + "aproduct/get_product_color";
    public static String PCOLOR_LIST_ARRAY = "pcolor_list";
    public static String PCOLOR_LIST_COLOR_ID = "color_id";
    public static String PCOLOR_LIST_COLOR_NAME = "color_name";

    /*Filter Sizes*/
    public static String PSIZE_LIST_URL = main_url + "aproduct/get_product_sizes/";
    public static String PSIZE_LIST_ARRAY = "psize_list";
    public static String PSIZE_LIST_SIZE_ID = "size_id";
    public static String PSIZE_LIST_SIZE = "size";

    public static String MEN_FULL_TSHIRT_ARRAY = "products";
    public static String MEN_FULL_TSHIRT_BASE_URL = "base_url";
    public static String MEN_FULL_TSHIRT_PRODUCT_IMAGE_URL = "image_url";
    public static String MEN_FULL_TSHIRT_PID = "pid";
    public static String MEN_FULL_TSHIRT_PNAME = "pname";
    public static String MEN_FULL_TSHIRT_PCCAT_ID = "pccat_id";
    public static String MEN_FULL_TSHIRT_FKP_SIZE_ID = "fk_psize_ids";
    public static String MEN_FULL_TSHIRT_DISCOUNT = "discount";
    public static String MEN_FULL_TSHIRT_MRP = "mrp";
    public static String MEN_FULL_TSHIRT_PRICE = "price";
    public static String MEN_FULL_TSHIRT_SIZE_ARRAY = "psize_list";
    public static String MEN_FULL_TSHIRT_SIZE_ID = "size_id";
    public static String MEN_FULL_TSHIRT_SIZE = "size";
    public static String MEN_FULL_TSHIRT_COLOR_ID = "fk_pcolor_id";
    public static String Filtered_Product_List = main_url + "aproduct/product_filteration";

    /*****************************************************
     *  customer Contact Us
     *************************************************/
    public static String contact_us_url = main_url + "home/send_email_for_contact_us";
    public static String contact_us_name = "name";
    public static String contact_us_email = "email";
    public static String contact_us_mobile = "mobile";
    public static String contact_us_subject = "subject";
    public static String contact_us_message = "message";

    /*****************************************************
     * Check in stock products
     *************************************************/
    public static String in_stock_products_url = main_url + "aorder/get_stock_details";
    public static String cart_product_details = "product_details";

    /*****************************************************
     * Get response from Check in stock products
     *************************************************/
    public static String in_stock_stock_details_array = "stock_details";
    public static String in_stock_stock_product_id = "id";
    public static String in_stock_product_size_id = "size_id";
    public static String in_stock_stock_product_status = "status";
    public static String in_stock_stock_cgst = "cgst";
    public static String in_stock_stock_sgst = "sgst";
    /*****************************************************
     * New Arrival Home Screen
     *************************************************/
    public static String new_arrival_url = main_url + "aproduct/new_arrival_products";
    public static String new_arrival_ARRAY = "new_arrival";
    public static String new_arrival_PRODUCT_IMAGE_URL = "image_url";
    public static String new_arrival_PID = "pid";
    public static String new_arrival_PNAME = "pname";
    public static String new_arrival_PCCAT_ID = "pccat_id";
    public static String new_arrival_PRICE = "price";
    public static String new_arrival_COLOR_ID = "fk_pcolor_id";

    /*****************************************************
     * Deals of the day
     *************************************************/
    public static String deal_day_products_home_url = main_url + "aproduct/deals_of_the_day_products_home";
    public static String deal_day_products_list_url = main_url + "aproduct/deals_of_the_day_products";
    public static String deal_day_products_array = "deal_day_products";
    public static String deal_day_products_deal_product_id = "deal_product_id";
    public static String deal_day_products_deal_pname = "pname";
    public static String deal_day_products_deal_price = "deal_price";
    public static String deal_day_products_deal_image_url = "image_url";
    public static String deal_day_products_deal_fk_pcolor_id = "fk_pcolor_id";
    public static String deal_day_products_deal_prod_price = "prod_price";
    public static String deal_day_products_deal_discount = "discount";
    public static String deal_day_products_deal_pccat_id = "pccat_id";
    public static String deal_day_products_deal_child_cat_name = "child_cat_name";
    public static String deal_day_products_deal_status = "deal_status";

    /*****************************************************
     * Recommended Prodects
     *************************************************/
    public static String recommended_products_url = main_url + "aproduct/recommended_products/";
    public static String recommended_products_array = "recommended_products";
    public static String recommended_products_pid = "pid";
    public static String recommended_products_pccat_id = "pccat_id";
    public static String recommended_products_fk_pcolor_id = "fk_pcolor_id";
    public static String recommended_products_pname = "pname";
    public static String recommended_products_mrp = "mrp";
    public static String recommended_products_price = "price";
    public static String recommended_products_image_url = "image_url";
    public static String recommended_products_discount = "discount";
    public static String recommended_products_deal_status = "deal_status";
    public static String recommended_products_fk_psize_ids = "fk_psize_ids";
    /*****************************************************
     * Apply coupon code
     *************************************************/
    public static String apply_coupon_code_url = main_url + "promo/apply_coupon_code";
    public static String apply_coupon_code_str = "coupon_code";
    public static String apply_coupon_code_cust_id = "cust_id";
    public static String apply_coupon_code_product_details_coupon = "product_details_coupon";

    /*****************************************************
     * Refund
     *************************************************/
    public static String refund_url = main_url + "aorder/get_refund_order_data";
    public static String refund_order_id = "order_id";
    public static String refund_cust_id = "cust_id";
    public static String refund_product_details = "product_details";
    public static String refund_return_reason = "return_reason";
    /*Refund Data*/
    public static String refund_order_data_array = "refund_order_data";
    public static String refund_order_data_refund_id = "refund_id";
    public static String refund_order_data_cust_id = "cust_id";
    public static String refund_order_data_first_name = "first_name";
    public static String refund_order_data_last_name = "last_name";
    public static String refund_order_data_order_id = "order_id";
    public static String refund_order_data_address = "address";
    public static String refund_order_data_area = "area";
    public static String refund_order_data_city = "city";
    public static String refund_order_data_state = "state";
    public static String refund_order_data_pincode = "pincode";


    public static String refund_prod_data_array = "refund_prod_data";
    public static String refund_prod_data_prod_name = "prod_name";
    public static String refund_prod_data_prod_size = "size";
    public static String refund_prod_data_prod_price = "prod_price";
    public static String refund_prod_data_prod_image_url = "prod_image_url";
    public static String refund_prod_data_quantity = "quantity";
    public static String refund_prod_data_amount = "amount";

    /*****************************************************
     * Refunded product confirmation
     *************************************************/
    public static String refund_order_otp_confirmation_url = main_url + "aorder/generate_otp_refund";
    public static String refund_order_otp_cust_id = "cust_id";

    /*****************************************************
     * Offers secction
     *************************************************/
    public static String offers_url = main_url + "promo/get_offers";
    public static String offers_array = "offers_data";
    public static String offers_Name = "promocode";
    public static String offers_Image_Url = "promo_image_url";
    public static String offers_Details = "description";

}


