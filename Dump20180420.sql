-- MySQL dump 10.13  Distrib 5.6.24, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: wjmartz_new
-- ------------------------------------------------------
-- Server version	5.5.59

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address_type`
--

DROP TABLE IF EXISTS `address_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_type` (
  `address_type_id` int(1) NOT NULL AUTO_INCREMENT,
  `address_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`address_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_type`
--

LOCK TABLES `address_type` WRITE;
/*!40000 ALTER TABLE `address_type` DISABLE KEYS */;
INSERT INTO `address_type` VALUES (1,'Home'),(2,'Work'),(3,'Other');
/*!40000 ALTER TABLE `address_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `area`
--

DROP TABLE IF EXISTS `area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area` (
  `area_id` int(5) NOT NULL AUTO_INCREMENT,
  `area_name` varchar(20) NOT NULL,
  `city_id` int(5) NOT NULL,
  `pincode_id` int(5) NOT NULL,
  PRIMARY KEY (`area_id`),
  KEY `city_id` (`city_id`),
  KEY `pincode_id` (`pincode_id`),
  CONSTRAINT `area_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `city` (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area`
--

LOCK TABLES `area` WRITE;
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
INSERT INTO `area` VALUES (1,'Aundh',1,7),(2,'Bhosari',1,34),(3,'Katraj',1,41),(5,'Pune Camp',1,5),(7,'Pune Gaothan',1,5),(8,'Koregoan Park',1,1),(9,'Pune MG Road',1,1),(10,'Poolgate',1,1),(11,'Bund Gaden',1,1),(12,'North Main Road',1,1),(13,'Shanti Kunj Station ',1,1),(14,'Shukarwarpeth',1,2),(15,'Pune Nanapeth',1,2),(16,'Swargate',1,2),(17,'Raviwarpeth ',1,2),(18,'Wakdewadi',1,3),(19,'Khadki Bazar',1,3),(20,'Sangamwadi',1,3),(21,'Khadki ',1,3),(22,'Deccan Gymkhana',1,4),(23,'Bhandarkar Road',1,4),(24,'Shivajinagar',1,4),(25,'Law college road',1,4),(26,'Karve road',1,4),(27,'Prabhat road',1,4),(28,'Pune corporation',1,5),(29,'Yerwada - Phulenagar',1,6),(30,'Yerwada - Airport',1,6),(31,'Yerwada - Ishanya ma',1,6),(32,'Sakal Nagar ',1,7),(33,'Bhosale nagar',1,7),(34,'Shakarnagar',1,9),(35,'Pune - Satara Road',1,9),(36,'Sarasbagh - Patil pl',1,9),(37,'Rasthapeth',1,10),(38,'Managalwarpeth ',1,10),(39,'FC Road',1,10),(40,'Pune CME',1,11),(41,'Magarpatta',1,12),(42,'Fatima nagar',1,12),(43,'Pimpri - Ambedkar ch',1,13),(44,'Kalyani nagar',1,13),(45,'Wadgaon sheri',1,13),(46,'Viman nagar',1,13),(47,'Kharadi',1,13),(48,'Mundhwa',1,13),(49,'Vishrantwadi',1,14),(50,'Digi camp',1,14),(51,'Dhanori - lakat naka',1,14),(52,'Ghokhale nagar',1,15),(53,'Kalewadi',1,16),(54,'Rahatani phata',1,16),(55,'Pimpri - Karachi cho',1,16),(56,'Pimpri - Vastu udyog',1,17),(57,'Sant tukaram nagar',1,17),(58,'Yashwant nagar',1,17),(59,'Chinchwad station ',1,18),(60,'Chinchwad - MIDC',1,18),(61,'Chinchwad - Thermax ',1,18),(62,'Pashan',1,20),(63,'Bhavdhan',1,20),(64,'Sangvi old',1,24),(65,'Sangvi new',1,24),(66,'Pimple saudagar',1,24),(67,'Wakad - Jagtap dairy',1,24),(68,'Mohamadwadi',1,25),(69,'Hadapsar - Handewadi',1,25),(70,'Hadapsar - Industria',1,25),(71,'Sasanenagar',1,25),(72,'Hadapsar',1,25),(73,'Hadapsar Road',1,25),(74,'Hadapsar - Gondhale ',1,25),(75,'Budhwarpeth',1,26),(76,'Naryanpeth',1,26),(77,'Navipeth - shastri r',1,26),(78,'Sadashiv peth',1,26),(79,'Duttawadi',1,26),(80,'Sadashivpeth - Perug',1,26),(81,'J.M Road',1,26),(82,'Tilak Road ',1,26),(83,'Marunji',1,26),(84,'Vidyanagar',1,27),(85,'Chinchwadgaon',1,28),(86,'Bhumkar Chowk',1,28),(87,'Walhekarwadi',1,28),(88,'Thergaon',1,28),(89,'Wakad - Dange chowk',1,28),(90,'Thathawade',1,28),(91,'Kasarwdi',1,29),(92,'Keshav nagar',1,31),(93,'Ghorpadi bazar',1,31),(94,'Bibewadi',1,32),(95,'Mukund nagar',1,32),(96,'Market yard',1,32),(97,'Wanaworie',1,35),(98,'Wanaworie - Kadari n',1,35),(99,'Bhavanipeth - Timber',1,36),(100,'Dhankawadi',1,38),(101,'Nigdi',1,39),(102,'Ambegaon',1,38),(103,'Nigdi sector 27',1,39),(104,'Yamuna nagar',1,39),(105,'Pancard club road',1,40),(106,'Balewadi ',1,40),(107,'Baner - Aditi mall',1,40),(108,'Katraj - Trimurthi c',1,41),(109,'NIBM road',1,46),(110,'Sukhsagar nagar ',1,46),(111,'Kondhwa',1,46),(112,'Hinjewadi',1,44),(113,'Wakad - Mankar chowk',1,44),(114,'Hinjewadi - Mann',1,44),(115,'Hinjewadi',1,44),(116,'Hinjewadi road',1,44);
/*!40000 ALTER TABLE `area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `brand_id` int(9) NOT NULL AUTO_INCREMENT,
  `brand` varchar(30) NOT NULL,
  `brand_img_url` varchar(500) NOT NULL,
  `brand_status` int(1) NOT NULL DEFAULT '1',
  `parent_cat_id` int(9) NOT NULL,
  PRIMARY KEY (`brand_id`),
  KEY `parent_cat_id` (`parent_cat_id`),
  CONSTRAINT `brands_ibfk_1` FOREIGN KEY (`parent_cat_id`) REFERENCES `parent_category` (`parent_cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (1,'fsd','uploads/brands/eacbe066ee306e764de93b01da93f319.jpg',1,3),(2,'afdgfgfg','uploads/brands/2d82b47f7f7805c78f6eab309e9d418e.png',1,2),(3,'safdf','uploads/brands/76613d96cf9c6988d7ff4a9a3bab960f.png',0,1);
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `child_category`
--

DROP TABLE IF EXISTS `child_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `child_category` (
  `child_cat_id` int(9) NOT NULL AUTO_INCREMENT,
  `child_cat_name` varchar(30) NOT NULL,
  `child_cat_img_url` varchar(500) NOT NULL,
  `parent_cat_id` int(9) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`child_cat_id`),
  KEY `sub_cat_id` (`parent_cat_id`),
  KEY `parent_cat_id` (`parent_cat_id`),
  CONSTRAINT `child_category_ibfk_1` FOREIGN KEY (`parent_cat_id`) REFERENCES `parent_category` (`parent_cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `child_category`
--

LOCK TABLES `child_category` WRITE;
/*!40000 ALTER TABLE `child_category` DISABLE KEYS */;
INSERT INTO `child_category` VALUES (0,'0','0',0,0),(1,'shoes','uploads/child_categories/511b243c1b4899e307ccd7ef06d1efcd.jpg',1,1),(2,'sgdfgb','uploads/child_categories/2dfc459698c3609bfb28568a83d57e9d.png',2,0);
/*!40000 ALTER TABLE `child_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `city_id` int(5) NOT NULL AUTO_INCREMENT,
  `city` varchar(20) NOT NULL,
  `state_id` int(5) NOT NULL,
  PRIMARY KEY (`city_id`),
  UNIQUE KEY `city` (`city`),
  KEY `state_id` (`state_id`),
  CONSTRAINT `city_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `state` (`state_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'Pune',1);
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `cust_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `mobile` bigint(10) NOT NULL,
  `password` varchar(45) NOT NULL DEFAULT 'wjmartz',
  `user_photo` varchar(200) NOT NULL,
  `user_status` int(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`cust_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `mobile` (`mobile`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_address`
--

DROP TABLE IF EXISTS `customer_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_address` (
  `address_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `cust_id` bigint(12) NOT NULL,
  `address` varchar(200) NOT NULL,
  `area` varchar(100) NOT NULL,
  `pincode` int(5) NOT NULL,
  `city_id` int(5) NOT NULL,
  `state_id` int(5) NOT NULL,
  `is_default` int(1) NOT NULL DEFAULT '0' COMMENT '1=default,0=not default',
  `address_type` int(1) NOT NULL,
  PRIMARY KEY (`address_id`),
  KEY `user_id` (`cust_id`),
  KEY `pincode_id` (`pincode`),
  KEY `address_type` (`address_type`),
  KEY `cust_id` (`cust_id`),
  KEY `locality` (`area`),
  KEY `ca_city_id` (`city_id`),
  KEY `ca_state_id` (`state_id`),
  CONSTRAINT `ca_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`city_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ca_state_id` FOREIGN KEY (`state_id`) REFERENCES `state` (`state_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `customer_address_ibfk_1` FOREIGN KEY (`cust_id`) REFERENCES `customer` (`cust_id`),
  CONSTRAINT `customer_address_ibfk_2` FOREIGN KEY (`pincode`) REFERENCES `pincode` (`pincode_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_address`
--

LOCK TABLES `customer_address` WRITE;
/*!40000 ALTER TABLE `customer_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deals_of_day_product`
--

DROP TABLE IF EXISTS `deals_of_day_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deals_of_day_product` (
  `deal_id` int(9) NOT NULL AUTO_INCREMENT,
  `deal_product_id` bigint(12) NOT NULL,
  `deal_price` int(6) NOT NULL,
  `discount` int(3) NOT NULL,
  `deal_image_url` varchar(200) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`deal_id`),
  UNIQUE KEY `deal_product_id` (`deal_product_id`),
  CONSTRAINT `deals_of_day_product_ibfk_1` FOREIGN KEY (`deal_product_id`) REFERENCES `products` (`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deals_of_day_product`
--

LOCK TABLES `deals_of_day_product` WRITE;
/*!40000 ALTER TABLE `deals_of_day_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `deals_of_day_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `delivery_boy`
--

DROP TABLE IF EXISTS `delivery_boy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery_boy` (
  `delivery_boy_id` int(9) NOT NULL AUTO_INCREMENT,
  `delivery_boy_name` varchar(20) NOT NULL,
  `address` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` bigint(10) NOT NULL,
  `delivery_boy_photo` varchar(200) DEFAULT NULL,
  `delivery_boy_status` int(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `created_by` int(9) NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`delivery_boy_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `mobile` (`mobile`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `delivery_boy_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `delivery_boy_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delivery_boy`
--

LOCK TABLES `delivery_boy` WRITE;
/*!40000 ALTER TABLE `delivery_boy` DISABLE KEYS */;
/*!40000 ALTER TABLE `delivery_boy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discounts`
--

DROP TABLE IF EXISTS `discounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discounts` (
  `disc_id` int(4) NOT NULL AUTO_INCREMENT,
  `disc_code` varchar(10) NOT NULL,
  `disc_description` varchar(200) NOT NULL,
  `disc_status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`disc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Product''s Discount Code Details';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discounts`
--

LOCK TABLES `discounts` WRITE;
/*!40000 ALTER TABLE `discounts` DISABLE KEYS */;
INSERT INTO `discounts` VALUES (0,'NA','NA',0);
/*!40000 ALTER TABLE `discounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `feedback_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` bigint(10) NOT NULL,
  `message` varchar(200) NOT NULL,
  PRIMARY KEY (`feedback_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `mobile` (`mobile`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_screen_section`
--

DROP TABLE IF EXISTS `home_screen_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `home_screen_section` (
  `screen_id` int(3) NOT NULL AUTO_INCREMENT,
  `parent_cat_id` int(9) NOT NULL,
  `section` varchar(15) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`screen_id`),
  KEY `parent_cat_id` (`parent_cat_id`),
  CONSTRAINT `home_screen_section_ibfk_1` FOREIGN KEY (`parent_cat_id`) REFERENCES `parent_category` (`parent_cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_screen_section`
--

LOCK TABLES `home_screen_section` WRITE;
/*!40000 ALTER TABLE `home_screen_section` DISABLE KEYS */;
/*!40000 ALTER TABLE `home_screen_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_category`
--

DROP TABLE IF EXISTS `main_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_category` (
  `main_cat_id` int(9) NOT NULL AUTO_INCREMENT,
  `main_cat_name` varchar(30) NOT NULL,
  `main_cat_img_url` varchar(200) NOT NULL,
  PRIMARY KEY (`main_cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_category`
--

LOCK TABLES `main_category` WRITE;
/*!40000 ALTER TABLE `main_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `main_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `order_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `cust_id` bigint(12) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `address_id` bigint(12) NOT NULL,
  `address` varchar(200) NOT NULL,
  `area` varchar(100) NOT NULL,
  `pincode` int(5) NOT NULL,
  `order_date` date NOT NULL,
  `ord_status_id` int(1) NOT NULL,
  `time_slot_id` int(9) NOT NULL,
  `refered_by` varchar(15) NOT NULL,
  `instruction` varchar(30) NOT NULL,
  `delivery_date` date NOT NULL,
  `promocode_applied` int(3) NOT NULL DEFAULT '0',
  `refund_id` bigint(12) DEFAULT NULL,
  `modified_by` int(9) NOT NULL DEFAULT '0',
  `modified_on` datetime DEFAULT NULL,
  `modified_by_cust` bigint(12) NOT NULL DEFAULT '0',
  `modified_on_cust` datetime DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `cust_id` (`cust_id`),
  KEY `address_id` (`address_id`),
  KEY `ord_status_id` (`ord_status_id`),
  KEY `modified_by` (`modified_by`),
  KEY `time_slot_id` (`time_slot_id`),
  KEY `cust_id_2` (`cust_id`),
  KEY `modified_by_cust` (`modified_by_cust`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `order_ibfk_10` FOREIGN KEY (`cust_id`) REFERENCES `customer` (`cust_id`),
  CONSTRAINT `order_ibfk_5` FOREIGN KEY (`ord_status_id`) REFERENCES `order_status` (`ord_status_id`),
  CONSTRAINT `order_ibfk_6` FOREIGN KEY (`modified_by`) REFERENCES `user` (`id`),
  CONSTRAINT `order_ibfk_9` FOREIGN KEY (`time_slot_id`) REFERENCES `time_slots` (`time_slot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_details` (
  `order_details_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(12) NOT NULL,
  `product_id` bigint(12) NOT NULL DEFAULT '0',
  `size_id` int(3) NOT NULL,
  `price` int(6) unsigned NOT NULL,
  `quantity` varchar(15) NOT NULL,
  `total_amount` int(6) unsigned NOT NULL,
  PRIMARY KEY (`order_details_id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `size_id` (`size_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_details`
--

LOCK TABLES `order_details` WRITE;
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_status`
--

DROP TABLE IF EXISTS `order_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_status` (
  `ord_status_id` int(1) NOT NULL AUTO_INCREMENT,
  `order_status` varchar(20) NOT NULL,
  PRIMARY KEY (`ord_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_status`
--

LOCK TABLES `order_status` WRITE;
/*!40000 ALTER TABLE `order_status` DISABLE KEYS */;
INSERT INTO `order_status` VALUES (1,'In Process'),(2,'Ready To Dispatch'),(3,'Delivered'),(4,'Cancelled'),(5,'Failed');
/*!40000 ALTER TABLE `order_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `package`
--

DROP TABLE IF EXISTS `package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package` (
  `package_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(30) NOT NULL,
  `package_price` double(7,2) NOT NULL,
  `package_img` varchar(500) NOT NULL,
  `package_status` int(1) NOT NULL DEFAULT '1' COMMENT '1=active,0=inactive',
  `product_type` int(1) NOT NULL DEFAULT '2',
  `created_on` datetime NOT NULL,
  `created_by` int(9) NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`package_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `package_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `package_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package`
--

LOCK TABLES `package` WRITE;
/*!40000 ALTER TABLE `package` DISABLE KEYS */;
INSERT INTO `package` VALUES (0,'0',0.00,'',1,2,'0000-00-00 00:00:00',0,NULL,0);
/*!40000 ALTER TABLE `package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `package_addons`
--

DROP TABLE IF EXISTS `package_addons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_addons` (
  `package_addon_id` int(9) NOT NULL AUTO_INCREMENT,
  `package_addon` varchar(30) NOT NULL,
  `addon_description` varchar(5000) NOT NULL,
  `addon_img` varchar(500) NOT NULL,
  `addon_quantity` varchar(15) NOT NULL,
  `addon_price` double(7,2) NOT NULL,
  `in_stock` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`package_addon_id`)
) ENGINE=InnoDB AUTO_INCREMENT=414 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package_addons`
--

LOCK TABLES `package_addons` WRITE;
/*!40000 ALTER TABLE `package_addons` DISABLE KEYS */;
/*!40000 ALTER TABLE `package_addons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `package_products`
--

DROP TABLE IF EXISTS `package_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_products` (
  `package_product_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `package_product` varchar(30) NOT NULL,
  `package_product_img` varchar(500) NOT NULL,
  `prod_price` double(7,2) NOT NULL,
  `prod_quantity` varchar(15) NOT NULL,
  `package_id` bigint(12) NOT NULL,
  `in_stock` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`package_product_id`),
  KEY `package_id` (`package_id`),
  CONSTRAINT `package_products_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `package` (`package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package_products`
--

LOCK TABLES `package_products` WRITE;
/*!40000 ALTER TABLE `package_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `package_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parent_category`
--

DROP TABLE IF EXISTS `parent_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parent_category` (
  `parent_cat_id` int(9) NOT NULL AUTO_INCREMENT,
  `parent_cat_name` varchar(30) NOT NULL,
  `parent_cat_img_url` varchar(200) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`parent_cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parent_category`
--

LOCK TABLES `parent_category` WRITE;
/*!40000 ALTER TABLE `parent_category` DISABLE KEYS */;
INSERT INTO `parent_category` VALUES (0,'0','0',0),(1,'Footwears','uploads/parent_categories/d2ecb686a3894b18cdfc69faa1391da2.jpg',1),(2,'Women zone','uploads/parent_categories/12fe516c19178066b3ca460c19f9131e.jpg',1),(3,'Dresses','uploads/parent_categories/2af7b2cd530a364220c605bebf973e19.jpg',1);
/*!40000 ALTER TABLE `parent_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_details`
--

DROP TABLE IF EXISTS `payment_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_details` (
  `payment_details_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(12) NOT NULL,
  `mihpayid` varchar(30) NOT NULL,
  `payment_mode` varchar(10) NOT NULL,
  `transaction_id` varchar(30) NOT NULL,
  `bank_ref_no` varchar(30) NOT NULL,
  `amount` int(6) unsigned NOT NULL,
  `discount_amount` int(6) NOT NULL,
  `payment_datetime` datetime NOT NULL,
  `payment_status` varchar(30) NOT NULL,
  PRIMARY KEY (`payment_details_id`),
  KEY `payment_mode_id` (`payment_mode`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `payment_details_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_details`
--

LOCK TABLES `payment_details` WRITE;
/*!40000 ALTER TABLE `payment_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_mode`
--

DROP TABLE IF EXISTS `payment_mode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_mode` (
  `pay_mode_id` int(2) NOT NULL AUTO_INCREMENT,
  `payment_mode` varchar(30) NOT NULL,
  PRIMARY KEY (`pay_mode_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_mode`
--

LOCK TABLES `payment_mode` WRITE;
/*!40000 ALTER TABLE `payment_mode` DISABLE KEYS */;
INSERT INTO `payment_mode` VALUES (0,'0'),(1,'Credit_Card'),(2,'Debit_Card'),(3,'Cash On Delivery');
/*!40000 ALTER TABLE `payment_mode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personalised_package`
--

DROP TABLE IF EXISTS `personalised_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personalised_package` (
  `per_package_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `package_main_product` varchar(30) NOT NULL,
  `main_product_img` varchar(500) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `created_by` int(9) NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`per_package_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `personalised_package_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `personalised_package_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personalised_package`
--

LOCK TABLES `personalised_package` WRITE;
/*!40000 ALTER TABLE `personalised_package` DISABLE KEYS */;
/*!40000 ALTER TABLE `personalised_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personalised_package_products`
--

DROP TABLE IF EXISTS `personalised_package_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personalised_package_products` (
  `sub_product_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `per_package_id` bigint(12) NOT NULL,
  `product_name` varchar(30) NOT NULL,
  `product_description` varchar(5000) NOT NULL,
  `per_product_img` varchar(500) NOT NULL,
  `quantity` varchar(15) NOT NULL,
  `product_price` double(7,2) NOT NULL,
  `instruction` varchar(15) NOT NULL,
  `product_type` int(1) NOT NULL DEFAULT '4',
  `in_stock` int(1) NOT NULL DEFAULT '1',
  `is_addon` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sub_product_id`),
  KEY `per_package_id` (`per_package_id`),
  CONSTRAINT `personalised_package_products_ibfk_1` FOREIGN KEY (`per_package_id`) REFERENCES `personalised_package` (`per_package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=335 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personalised_package_products`
--

LOCK TABLES `personalised_package_products` WRITE;
/*!40000 ALTER TABLE `personalised_package_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `personalised_package_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pincode`
--

DROP TABLE IF EXISTS `pincode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pincode` (
  `pincode_id` int(5) NOT NULL AUTO_INCREMENT,
  `pincode` varchar(20) NOT NULL,
  `delivery_charges` int(6) NOT NULL,
  PRIMARY KEY (`pincode_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pincode`
--

LOCK TABLES `pincode` WRITE;
/*!40000 ALTER TABLE `pincode` DISABLE KEYS */;
INSERT INTO `pincode` VALUES (0,'0',0),(1,'411001',25),(2,'411002',25),(3,'411003',25),(4,'411004',25),(5,'411005',25),(6,'411006',25),(7,'411007',25),(8,'411008',25),(9,'411009',25),(10,'411011',25),(11,'411012',25),(12,'411013',25),(13,'411014',25),(14,'411015',25),(15,'411016',25),(16,'411017',25),(17,'411018',25),(18,'411019',25),(19,'411020',25),(20,'411021',25),(21,'411022',25),(22,'411023',25),(23,'411026',25),(24,'411027',25),(25,'411028',25),(26,'411030',25),(27,'411032',25),(28,'411033',25),(29,'411034',25),(30,'411035',25),(31,'411036',25),(32,'411037',25),(33,'411038',25),(34,'411039',25),(35,'411040',0),(36,'411041',25),(37,'411042',25),(38,'411043',25),(39,'411044',25),(40,'411045',25),(41,'411046',25),(42,'411051',25),(43,'411052',25),(44,'411057',25),(45,'411058',25),(46,'411048',25),(48,'411040',25);
/*!40000 ALTER TABLE `pincode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_colors`
--

DROP TABLE IF EXISTS `product_colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_colors` (
  `color_id` int(2) NOT NULL AUTO_INCREMENT,
  `color_name` varchar(20) NOT NULL,
  `color_code` varchar(7) NOT NULL,
  PRIMARY KEY (`color_id`),
  UNIQUE KEY `color_name` (`color_name`),
  UNIQUE KEY `color_name_2` (`color_name`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_colors`
--

LOCK TABLES `product_colors` WRITE;
/*!40000 ALTER TABLE `product_colors` DISABLE KEYS */;
INSERT INTO `product_colors` VALUES (1,'Beige','#F5F5DC'),(2,'Black','#292421'),(3,'Blue','#33A1DE'),(4,'Brown','#5C3317'),(5,'Dark Blue','#043E84'),(6,'Dark Green','#007C1D'),(7,'Gold','#FFD700'),(8,'Green','#00AF33'),(9,'Grey','#787878'),(10,'Light Blue','#44B8EA'),(11,'Light Green','#AED33C'),(12,'Maroon','#800000'),(13,'Orange','#E47833'),(14,'Pink','#EE6AA7'),(15,'Purple','#5C246E'),(16,'Red','#C82536'),(17,'Silver','#C0C0C0'),(18,'White','#FFFFFF'),(19,'Yellow','#FFE600'),(20,'Multicolor','');
/*!40000 ALTER TABLE `product_colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_sizes`
--

DROP TABLE IF EXISTS `product_sizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_sizes` (
  `size_id` int(2) NOT NULL AUTO_INCREMENT,
  `size` varchar(20) NOT NULL,
  `parent_cat_id` int(9) NOT NULL,
  PRIMARY KEY (`size_id`),
  UNIQUE KEY `size` (`size`),
  UNIQUE KEY `size_2` (`size`),
  UNIQUE KEY `size_3` (`size`),
  KEY `parent_cat_id` (`parent_cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_sizes`
--

LOCK TABLES `product_sizes` WRITE;
/*!40000 ALTER TABLE `product_sizes` DISABLE KEYS */;
INSERT INTO `product_sizes` VALUES (1,'S',2),(2,' M',2),(3,' L',2),(4,' XL',2),(5,' XXL',2),(6,'XXXL',2),(7,'4XL',2),(8,'XS',2),(9,'Free-Size',2),(10,'36',1),(11,'37',1),(12,'38',1),(13,'39',1),(14,'40',1),(15,'41',1),(16,'35',1),(17,'42',1);
/*!40000 ALTER TABLE `product_sizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_slot_quantity`
--

DROP TABLE IF EXISTS `product_slot_quantity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_slot_quantity` (
  `slot_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `product_id` bigint(12) NOT NULL,
  `size_id` int(2) NOT NULL,
  `quantity` int(9) unsigned NOT NULL,
  PRIMARY KEY (`slot_id`),
  KEY `product_id` (`product_id`),
  KEY `product_id_2` (`product_id`),
  KEY `product_id_3` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_slot_quantity`
--

LOCK TABLES `product_slot_quantity` WRITE;
/*!40000 ALTER TABLE `product_slot_quantity` DISABLE KEYS */;
INSERT INTO `product_slot_quantity` VALUES (1,1,3,1),(2,1,2,4),(3,2,10,0),(4,2,15,0);
/*!40000 ALTER TABLE `product_slot_quantity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_type`
--

DROP TABLE IF EXISTS `product_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_type` (
  `product_type_id` int(2) NOT NULL AUTO_INCREMENT,
  `product_type` varchar(20) NOT NULL,
  PRIMARY KEY (`product_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_type`
--

LOCK TABLES `product_type` WRITE;
/*!40000 ALTER TABLE `product_type` DISABLE KEYS */;
INSERT INTO `product_type` VALUES (1,'product'),(2,'package'),(4,'personalised_package');
/*!40000 ALTER TABLE `product_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_types`
--

DROP TABLE IF EXISTS `product_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_types` (
  `ptype_id` int(3) NOT NULL AUTO_INCREMENT,
  `ptype` varchar(20) NOT NULL,
  PRIMARY KEY (`ptype_id`),
  UNIQUE KEY `product_type` (`ptype`),
  UNIQUE KEY `ptype` (`ptype`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_types`
--

LOCK TABLES `product_types` WRITE;
/*!40000 ALTER TABLE `product_types` DISABLE KEYS */;
INSERT INTO `product_types` VALUES (1,'Full Sleeve'),(2,'Half Sleeve'),(0,'NA');
/*!40000 ALTER TABLE `product_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `prod_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `fk_pbrand_id` int(9) NOT NULL COMMENT 'product brand id',
  `fk_pcat_id` int(9) NOT NULL,
  `fk_pchild_id` int(9) NOT NULL COMMENT 'product child category id',
  `fk_ptype_id` int(3) NOT NULL DEFAULT '0' COMMENT 'product type id',
  `prod_name` varchar(50) NOT NULL,
  `fk_pcolor_id` int(2) NOT NULL,
  `fabric_type` varchar(10) NOT NULL,
  `fk_psize_ids` varchar(50) NOT NULL,
  `prod_mrp` int(6) unsigned NOT NULL,
  `prod_price` int(6) unsigned NOT NULL,
  `pwholesale_price` int(6) unsigned NOT NULL,
  `prod_fit_details` varchar(2000) DEFAULT NULL,
  `prod_fabric_details` varchar(2000) DEFAULT NULL,
  `prod_description` varchar(2000) NOT NULL,
  `delivery_n_return` varchar(2000) NOT NULL,
  `prod_image_url` varchar(200) NOT NULL,
  `prod_image_urls` varchar(3000) NOT NULL,
  `size_chart_image_url` varchar(200) DEFAULT NULL,
  `in_stock` int(1) NOT NULL DEFAULT '1',
  `fk_disc_id` int(4) NOT NULL DEFAULT '0' COMMENT 'discount Id',
  `discount` int(3) unsigned NOT NULL DEFAULT '0',
  `sgst` float(4,2) NOT NULL,
  `cgst` float(4,2) NOT NULL,
  `igst` float(4,2) NOT NULL DEFAULT '0.00',
  `deal_status` int(1) NOT NULL DEFAULT '0',
  `refund_status` int(1) NOT NULL DEFAULT '0',
  `created_by` int(9) NOT NULL COMMENT 'user id',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(9) DEFAULT NULL COMMENT 'user id',
  `modified_on` datetime DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`prod_id`),
  UNIQUE KEY `prod_name` (`prod_name`),
  KEY `fk_pbrand_id` (`fk_pbrand_id`,`fk_pchild_id`,`fk_ptype_id`,`fk_disc_id`,`created_by`,`modified_by`),
  KEY `fk_pchild_id` (`fk_pchild_id`),
  KEY `fk_ptype_id` (`fk_ptype_id`),
  KEY `fk_disc_id` (`fk_disc_id`),
  KEY `created_by` (`created_by`),
  KEY `fk_pcolor_id` (`fk_pcolor_id`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`fk_pbrand_id`) REFERENCES `brands` (`brand_id`),
  CONSTRAINT `products_ibfk_3` FOREIGN KEY (`fk_ptype_id`) REFERENCES `product_types` (`ptype_id`),
  CONSTRAINT `products_ibfk_4` FOREIGN KEY (`fk_pcolor_id`) REFERENCES `product_colors` (`color_id`),
  CONSTRAINT `products_ibfk_5` FOREIGN KEY (`fk_disc_id`) REFERENCES `discounts` (`disc_id`),
  CONSTRAINT `products_ibfk_6` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `products_ibfk_7` FOREIGN KEY (`modified_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,2,3,2,0,'fghghg',18,'Cotton','3,2',200,200,150,'gfhg','hgh','fhgh','fhfg','uploads/products/545cbb6bd3cf8efea66452fd83ad5987.jpg','uploads/products/8bba743b54f9b85cb1bbc371ab0036b4.jpg,uploads/products/63785d68a0b7a2f21e0c8a017d52be21.jpg,uploads/products/9f1fdca2b56ae490906c818c8edf9a55.jpg,uploads/products/dce25978cb28f45848b5ddf7ddb98e7c.jpg,uploads/products/3cbac419f76bd30cbd0797f548f5ed15.jpg','uploads/sizechart/e4cf8690700ac7c114bc274a6bf98c51.jpg',1,0,0,4.50,4.50,0.00,0,0,1,'2018-04-19 13:09:54',NULL,NULL,0),(2,3,1,1,0,'afdf',3,'Casual Sho','10,15',200,200,200,'bb',NULL,'fgbf','fgbgb','uploads/products/d5bf962241b15c20932881156c645529.jpg','uploads/products/97183e1120b18abef1ca2c9d4f88e8ff.jpg,uploads/products/24bcd4baf961d5c5fe292d7ff2c13a51.jpg,uploads/products/26e59a6b52e463a441d42bed9a6e45dc.jpg,uploads/products/ef0b712b7b88a0b1233d8830cc9ad77a.jpg,uploads/products/d4cff700fd5b27556400d14cf7a399d7.jpg',NULL,1,0,0,4.50,4.50,0.00,0,0,1,'2018-04-19 17:45:58',NULL,NULL,1);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promocode`
--

DROP TABLE IF EXISTS `promocode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promocode` (
  `promo_id` int(3) NOT NULL AUTO_INCREMENT,
  `promo_type_id` int(3) NOT NULL,
  `child_cat_id` int(9) NOT NULL,
  `promocode` varchar(10) NOT NULL,
  `discount` int(3) NOT NULL,
  `applicable_promo` int(3) NOT NULL DEFAULT '0',
  `applied_promo` int(11) NOT NULL DEFAULT '0',
  `description` varchar(500) NOT NULL,
  `date` date DEFAULT NULL,
  `promo_image_url` varchar(200) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`promo_id`),
  UNIQUE KEY `promocode` (`promocode`),
  KEY `promo_type_id` (`promo_type_id`),
  KEY `child_cat_id` (`child_cat_id`),
  CONSTRAINT `promocode_ibfk_3` FOREIGN KEY (`promo_type_id`) REFERENCES `promocode_type` (`promo_type_id`),
  CONSTRAINT `promocode_ibfk_4` FOREIGN KEY (`child_cat_id`) REFERENCES `child_category` (`child_cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promocode`
--

LOCK TABLES `promocode` WRITE;
/*!40000 ALTER TABLE `promocode` DISABLE KEYS */;
INSERT INTO `promocode` VALUES (1,4,0,'abc',12,0,0,'hgkhk',NULL,'uploads/promocodes/1fd17fe4322f3934532f0c307039b84c.jpg',1),(2,3,0,'gkjhg',4,0,0,'gykh',NULL,'uploads/promocodes/c6e9d22c7a6c8c28560e69dec2b6e951.jpg',1),(3,3,0,'zsc',12,0,0,'df',NULL,'uploads/promocodes/8c3efd884d91224740c9bca2fdda6c7d.jpg',1);
/*!40000 ALTER TABLE `promocode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promocode_type`
--

DROP TABLE IF EXISTS `promocode_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promocode_type` (
  `promo_type_id` int(3) NOT NULL AUTO_INCREMENT,
  `promocode_type` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`promo_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promocode_type`
--

LOCK TABLES `promocode_type` WRITE;
/*!40000 ALTER TABLE `promocode_type` DISABLE KEYS */;
INSERT INTO `promocode_type` VALUES (1,'Userwise',1),(2,'Datewise',1),(3,'permant',1),(4,'New Customer',1);
/*!40000 ALTER TABLE `promocode_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `refund_order`
--

DROP TABLE IF EXISTS `refund_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refund_order` (
  `refund_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(12) NOT NULL,
  `cust_id` bigint(12) NOT NULL,
  `address` varchar(200) NOT NULL,
  `area` varchar(100) NOT NULL,
  `pincode` int(5) NOT NULL,
  `mihpayid` varchar(30) NOT NULL,
  `order_date` date NOT NULL,
  `delivery_date` date NOT NULL,
  `refund_date` date NOT NULL,
  `amount` int(6) NOT NULL,
  `discount_amount` int(6) NOT NULL,
  `return_reason` varchar(200) NOT NULL,
  `modified_by` int(9) NOT NULL DEFAULT '0',
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`refund_id`),
  KEY `order_id` (`order_id`),
  KEY `cust_id` (`cust_id`),
  KEY `pincode` (`pincode`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `refund_order_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `user` (`id`),
  CONSTRAINT `refund_order_ibfk_6` FOREIGN KEY (`cust_id`) REFERENCES `customer` (`cust_id`),
  CONSTRAINT `refund_order_ibfk_7` FOREIGN KEY (`pincode`) REFERENCES `pincode` (`pincode_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refund_order`
--

LOCK TABLES `refund_order` WRITE;
/*!40000 ALTER TABLE `refund_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `refund_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `refund_order_details`
--

DROP TABLE IF EXISTS `refund_order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refund_order_details` (
  `refund_ord_details_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `refund_id` bigint(12) NOT NULL,
  `product_id` bigint(12) NOT NULL,
  `size_id` int(3) NOT NULL,
  `price` int(6) NOT NULL,
  `quantity` varchar(20) NOT NULL,
  `amount` int(6) NOT NULL,
  `sgst_amount` int(6) NOT NULL,
  `cgst_amount` int(6) NOT NULL,
  PRIMARY KEY (`refund_ord_details_id`),
  KEY `refund_id` (`refund_id`),
  KEY `product_id` (`product_id`),
  KEY `size_id` (`size_id`),
  CONSTRAINT `refund_order_details_ibfk_1` FOREIGN KEY (`refund_id`) REFERENCES `refund_order` (`refund_id`),
  CONSTRAINT `refund_order_details_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refund_order_details`
--

LOCK TABLES `refund_order_details` WRITE;
/*!40000 ALTER TABLE `refund_order_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `refund_order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slider` (
  `slider_id` int(9) NOT NULL AUTO_INCREMENT,
  `image_path` varchar(200) NOT NULL,
  `created_on` datetime NOT NULL,
  `created_by` int(9) NOT NULL DEFAULT '0',
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`slider_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `slider_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `slider_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider`
--

LOCK TABLES `slider` WRITE;
/*!40000 ALTER TABLE `slider` DISABLE KEYS */;
INSERT INTO `slider` VALUES (1,'uploads/sliders/android/1slider.jpg','2017-08-04 00:00:00',0,'2018-04-19 19:16:56',1),(2,'uploads/sliders/android/0028a92a8755a24e0043a241c271fba71.jpg','2017-08-04 00:00:00',0,'2018-04-19 19:17:35',1),(3,'static_img/uploads/sliders/Untitled-7.jpg','2017-08-04 00:00:00',0,'2017-11-25 15:39:21',1),(5,'static_img/uploads/sliders/Untitled-19.jpg','2017-09-13 00:00:00',1,'2017-12-01 15:23:07',1),(6,'static_img/uploads/sliders/Untitled-39.jpg','2017-09-13 00:00:00',1,'2017-12-01 15:23:34',1);
/*!40000 ALTER TABLE `slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sliders` (
  `slider_img_id` int(5) NOT NULL AUTO_INCREMENT,
  `slider_type` varchar(30) NOT NULL,
  `slider_image` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `cuser_id` int(9) NOT NULL,
  `created_on` datetime NOT NULL,
  `muser_id` int(9) NOT NULL DEFAULT '0',
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`slider_img_id`),
  KEY `si_cuser_id` (`cuser_id`),
  KEY `si_muser_id` (`muser_id`),
  CONSTRAINT `u_cuser_id` FOREIGN KEY (`cuser_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `u_muser_id` FOREIGN KEY (`muser_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sliders`
--

LOCK TABLES `sliders` WRITE;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` VALUES (1,'Android','upload/slider/android/appslider1.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(2,'Android','upload/slider/android/appslider2.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(3,'Android','upload/slider/android/appslider3.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(4,'Android','upload/slider/android/appslider4.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(5,'Android','upload/slider/android/appslider5.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(6,'Android','upload/slider/android/appslider6.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(7,'Android','upload/slider/android/appslider7.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(8,'Android','upload/slider/android/appslider8.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(9,'Android','upload/slider/android/appslider9.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(10,'Android','upload/slider/android/appslider10.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(11,'Web','upload/slider/web/webslider1.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(12,'Web','upload/slider/web/webslider2.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(13,'Web','upload/slider/web/webslider3.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(14,'Web','upload/slider/web/webslider4.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(15,'Web','upload/slider/web/webslider5.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(16,'Web','upload/slider/web/webslider6.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(17,'Web','upload/slider/web/webslider7.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(18,'Web','upload/slider/web/webslider8.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(19,'Web','upload/slider/web/webslider9.jpg',1,1,'2018-03-31 00:00:00',0,NULL),(20,'Web','upload/slider/web/webslider10.jpg',1,1,'2018-03-31 00:00:00',0,NULL);
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state` (
  `state_id` int(5) NOT NULL AUTO_INCREMENT,
  `state` varchar(20) NOT NULL,
  PRIMARY KEY (`state_id`),
  UNIQUE KEY `state` (`state`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state`
--

LOCK TABLES `state` WRITE;
/*!40000 ALTER TABLE `state` DISABLE KEYS */;
INSERT INTO `state` VALUES (1,'Maharashtra');
/*!40000 ALTER TABLE `state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `support_chat`
--

DROP TABLE IF EXISTS `support_chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `support_chat` (
  `support_chat_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `service_id` bigint(12) NOT NULL,
  `cust_id` bigint(12) NOT NULL,
  `message` varchar(5000) NOT NULL,
  PRIMARY KEY (`support_chat_id`),
  KEY `service_id` (`service_id`),
  KEY `cust_id` (`cust_id`),
  CONSTRAINT `support_chat_ibfk_3` FOREIGN KEY (`service_id`) REFERENCES `support_services` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `support_chat`
--

LOCK TABLES `support_chat` WRITE;
/*!40000 ALTER TABLE `support_chat` DISABLE KEYS */;
/*!40000 ALTER TABLE `support_chat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `support_services`
--

DROP TABLE IF EXISTS `support_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `support_services` (
  `service_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `support_service` varchar(50) NOT NULL,
  `service_description` varchar(5000) NOT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `support_services`
--

LOCK TABLES `support_services` WRITE;
/*!40000 ALTER TABLE `support_services` DISABLE KEYS */;
INSERT INTO `support_services` VALUES (1,'Products Not Deliverd','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. '),(2,'Tech Issue','Things not working as Expected?Just make sure your app is the latest version.if your problem persists,please provide the details of the issue here'),(3,'Suggest a Product','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'),(4,'What\'s your story?','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.');
/*!40000 ALTER TABLE `support_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_order`
--

DROP TABLE IF EXISTS `temp_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_order` (
  `ref_order_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `cust_id` bigint(12) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `address_id` bigint(12) NOT NULL,
  `address` varchar(200) NOT NULL,
  `area` varchar(100) NOT NULL,
  `pincode` int(5) NOT NULL,
  `order_date` date NOT NULL,
  `ord_status_id` int(1) NOT NULL,
  `time_slot_id` int(9) NOT NULL,
  `refered_by` varchar(15) NOT NULL,
  `instruction` varchar(30) NOT NULL,
  `delivery_date` date NOT NULL,
  `promocode_applied` int(3) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ref_order_id`),
  KEY `cust_id` (`cust_id`),
  KEY `address_id` (`address_id`),
  KEY `time_slot_id` (`time_slot_id`),
  KEY `ord_status_id` (`ord_status_id`),
  KEY `pincode` (`pincode`),
  CONSTRAINT `temp_order_ibfk_3` FOREIGN KEY (`time_slot_id`) REFERENCES `time_slots` (`time_slot_id`),
  CONSTRAINT `temp_order_ibfk_5` FOREIGN KEY (`ord_status_id`) REFERENCES `order_status` (`ord_status_id`),
  CONSTRAINT `temp_order_ibfk_6` FOREIGN KEY (`cust_id`) REFERENCES `customer` (`cust_id`),
  CONSTRAINT `temp_order_ibfk_7` FOREIGN KEY (`pincode`) REFERENCES `pincode` (`pincode_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_order`
--

LOCK TABLES `temp_order` WRITE;
/*!40000 ALTER TABLE `temp_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `temp_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_order_details`
--

DROP TABLE IF EXISTS `temp_order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_order_details` (
  `temp_order_detail_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `ref_order_id` bigint(12) NOT NULL,
  `product_id` bigint(12) NOT NULL DEFAULT '0',
  `size_id` int(3) NOT NULL,
  `price` int(6) unsigned NOT NULL,
  `quantity` varchar(20) NOT NULL,
  `sgst_amount` int(6) NOT NULL,
  `cgst_amount` int(6) NOT NULL,
  `amount` int(6) unsigned NOT NULL,
  `order_id` bigint(12) DEFAULT NULL,
  `rf_status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`temp_order_detail_id`),
  KEY `temp_order_id` (`ref_order_id`),
  KEY `product_id` (`product_id`),
  KEY `ref_order_id` (`ref_order_id`),
  KEY `product_id_2` (`product_id`),
  KEY `size_id` (`size_id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `temp_order_details_ibfk_3` FOREIGN KEY (`size_id`) REFERENCES `product_sizes` (`size_id`),
  CONSTRAINT `temp_order_details_ibfk_4` FOREIGN KEY (`product_id`) REFERENCES `products` (`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_order_details`
--

LOCK TABLES `temp_order_details` WRITE;
/*!40000 ALTER TABLE `temp_order_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `temp_order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_payment_details`
--

DROP TABLE IF EXISTS `temp_payment_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_payment_details` (
  `payment_details_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `ref_order_id` bigint(12) NOT NULL,
  `mihpayid` varchar(30) NOT NULL,
  `payment_mode` varchar(10) NOT NULL,
  `transaction_id` varchar(30) NOT NULL,
  `bank_ref_no` varchar(30) NOT NULL,
  `amount` int(6) unsigned NOT NULL,
  `discount_amount` int(6) NOT NULL,
  `payment_datetime` datetime NOT NULL,
  `payment_status` varchar(30) NOT NULL,
  `trans_reason` varchar(100) NOT NULL,
  PRIMARY KEY (`payment_details_id`),
  KEY `ref_order_id` (`ref_order_id`),
  KEY `payment_mode_id` (`payment_mode`),
  CONSTRAINT `temp_payment_details_ibfk_1` FOREIGN KEY (`ref_order_id`) REFERENCES `temp_order` (`ref_order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_payment_details`
--

LOCK TABLES `temp_payment_details` WRITE;
/*!40000 ALTER TABLE `temp_payment_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `temp_payment_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `time_slots`
--

DROP TABLE IF EXISTS `time_slots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_slots` (
  `time_slot_id` int(9) NOT NULL AUTO_INCREMENT,
  `time_slot` varchar(20) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `shift_status` int(1) NOT NULL,
  PRIMARY KEY (`time_slot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `time_slots`
--

LOCK TABLES `time_slots` WRITE;
/*!40000 ALTER TABLE `time_slots` DISABLE KEYS */;
INSERT INTO `time_slots` VALUES (1,'9 AM To 11 PM',1,1);
/*!40000 ALTER TABLE `time_slots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(50) NOT NULL,
  `user_type` int(2) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_type` (`user_type`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (0,'0','0','0',0,1),(1,'admin','Wilson@2018','admin@wjmartz.com',1,1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_type`
--

DROP TABLE IF EXISTS `user_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_type` (
  `user_type_id` int(2) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(20) NOT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_type`
--

LOCK TABLES `user_type` WRITE;
/*!40000 ALTER TABLE `user_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'wjmartz_new'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-20 11:31:03
